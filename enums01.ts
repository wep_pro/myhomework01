enum CardinalDirections {
    North = "North",
    East = "East",
    South = "South",
    West = "West"
}

let currentDirection = CardinalDirections.South;

console.log(currentDirection);